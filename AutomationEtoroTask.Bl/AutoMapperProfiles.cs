﻿using System;
using AutomationEtoroTask.Bl.DTOs;
using AutoMapper;
using OpenTidl.Models;

namespace AutomationEtoroTask.Bl
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<AlbumModel, AlbumModelDto>();
            CreateMap<UserSubscriptionModel, UserSubscriptionModelDto>();
            CreateMap<TrackModel, TrackModelDto>();
            CreateMap<UserModel, UserModelDto>();
            CreateMap<VideoModel, VideoModelDto>();
            CreateMap<StreamUrlModel, StreamUrlModelDto>();
            CreateMap<SessionModel, SessionModelDto>();
            CreateMap<PlaylistModel, PlaylistModelDto>();
            CreateMap<LoginModel, LoginModelDto>();
            CreateMap<EmptyModel, EmptyModelDto>();
            CreateMap<ClientModel, ClientModelDto>();
            CreateMap<ArtistModel, ArtistModelDto>();
            CreateMap<VideoStreamUrlModel, VideoStreamUrlModelDto>();
        }
    }
}
