﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using AutomationEtoroTask.Bl.SessionProvider;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl
{
    public class AutomationTaskSessionProvider : IAutomationTaskSessionProvider
    {
        private readonly IOpenSessionPlaylist _sessionPlaylist;
        private readonly IOpenSessionFavoritePlaylist _sessionFavoritePlaylist;
        private readonly IOpenSessionPlaylistTracks _sessionPlaylistTracks;
        private readonly IOpenSessionLogout _openSessionLogout;

        public AutomationTaskSessionProvider(IOpenSessionPlaylist sessionPlaylist, IOpenSessionFavoritePlaylist sessionFavoritePlaylist, IOpenSessionPlaylistTracks sessionPlaylistTracks, IOpenSessionLogout openSessionLogout)
        {
            _sessionPlaylist = sessionPlaylist;
            _sessionFavoritePlaylist = sessionFavoritePlaylist;
            _sessionPlaylistTracks = sessionPlaylistTracks;
            _openSessionLogout = openSessionLogout;
        }

        public PlaylistModelDto CreateUserPlaylist(string title, int? timeout)
        {
            return _sessionPlaylist.CreateUserPlaylist(title, timeout);
        }

        public Task<PlaylistModelDto> CreateUserPlaylist(string title)
        {
            return _sessionPlaylist.CreateUserPlaylist(title);
        }

        public Task<EmptyModelDto> DeletePlaylist(string playlistUuid, string playlistETag)
        {
            return _sessionPlaylist.DeletePlaylist(playlistUuid, playlistETag);
        }

        public EmptyModelDto DeletePlaylist(string playlistUuid, string playlistETag, int? timeout)
        {
            return _sessionPlaylist.DeletePlaylist(playlistUuid, playlistETag, timeout);
        }

        public EmptyModelDto UpdatePlaylist(string playlistUuid, string playlistETag, string title, int? timeout)
        {
            return _sessionPlaylist.UpdatePlaylist(playlistUuid, playlistETag, title, timeout);
        }

        public Task<EmptyModelDto> UpdatePlaylist(string playlistUuid, string playlistETag, string title)
        {
            return _sessionPlaylist.UpdatePlaylist(playlistUuid, playlistETag, title);
        }

        public JsonList<PlaylistModelDto> GetUserPlaylists(int? limit, int? timeout)
        {
            return _sessionPlaylist.GetUserPlaylists(limit, timeout);
        }

        public Task<JsonList<PlaylistModelDto>> GetUserPlaylists(int limit = 9999)
        {
            return _sessionPlaylist.GetUserPlaylists(limit);
        }

        public PlaylistModelDto GetPlaylist(string playlistUuid, int? timeout)
        {
            return _sessionPlaylist.GetPlaylist(playlistUuid, timeout);
        }

        public Task<PlaylistModelDto> GetPlaylist(string playlistUuid)
        {
            return _sessionPlaylist.GetPlaylist(playlistUuid);
        }

        public Task<EmptyModelDto> AddFavoritePlaylist(string playlistUuid)
        {
            return _sessionFavoritePlaylist.AddFavoritePlaylist(playlistUuid);
        }

        public EmptyModelDto AddFavoritePlaylist(string playlistUuid, int? timeout)
        {
            return _sessionFavoritePlaylist.AddFavoritePlaylist(playlistUuid, timeout);
        }

        public JsonList<JsonListItem<PlaylistModelDto>> GetFavoritePlaylists(int? limit, int? timeout)
        {
            return _sessionFavoritePlaylist.GetFavoritePlaylists(limit, timeout);
        }

        public Task<JsonList<JsonListItem<PlaylistModelDto>>> GetFavoritePlaylists(int limit = 9999)
        {
            return _sessionFavoritePlaylist.GetFavoritePlaylists(limit);
        }

        public EmptyModelDto RemoveFavoritePlaylist(string playlistUuid, int? timeout)
        {
            return _sessionFavoritePlaylist.RemoveFavoritePlaylist(playlistUuid, timeout);
        }

        public Task<EmptyModelDto> RemoveFavoritePlaylist(string playlistUuid)
        {
            return _sessionFavoritePlaylist.RemoveFavoritePlaylist(playlistUuid);
        }

        public Task<EmptyModelDto> AddPlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> trackIds, int toIndex = 0)
        {
            return _sessionPlaylistTracks.AddPlaylistTracks(playlistUuid, playlistETag, trackIds, toIndex);
        }

        public EmptyModelDto AddPlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> trackIds, int? toIndex,
            int? timeout)
        {
            return _sessionPlaylistTracks.AddPlaylistTracks(playlistUuid, playlistETag, trackIds, toIndex, timeout);
        }

        public Task<EmptyModelDto> DeletePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices)
        {
            return _sessionPlaylistTracks.DeletePlaylistTracks(playlistUuid, playlistETag, indices);
        }

        public EmptyModelDto DeletePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices, int? timeout)
        {
            return _sessionPlaylistTracks.DeletePlaylistTracks(playlistUuid, playlistETag, indices, timeout);
        }

        public JsonList<TrackModelDto> GetPlaylistTracks(string playlistUuid, int? offset, int? limit, int? timeout)
        {
            return _sessionPlaylistTracks.GetPlaylistTracks(playlistUuid, offset, limit, timeout);
        }

        public Task<JsonList<TrackModelDto>> GetPlaylistTracks(string playlistUuid, int offset = 0, int limit = 9999)
        {
            return _sessionPlaylistTracks.GetPlaylistTracks(playlistUuid, offset, limit);
        }

        public Task<EmptyModelDto> MovePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices, int toIndex = 0)
        {
            return _sessionPlaylistTracks.MovePlaylistTracks(playlistUuid, playlistETag, indices, toIndex);
        }

        public EmptyModelDto MovePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices, int? toIndex,
            int? timeout)
        {
            return _sessionPlaylistTracks.MovePlaylistTracks(playlistUuid, playlistETag, indices, toIndex, timeout);
        }

        public EmptyModelDto Logout(int? timeout)
        {
            return _openSessionLogout.Logout(timeout);
        }

        public Task<EmptyModelDto> Logout()
        {
            return _openSessionLogout.Logout();
        }
    }
}