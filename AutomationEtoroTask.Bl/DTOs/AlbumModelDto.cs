﻿using System;

namespace AutomationEtoroTask.Bl.DTOs
{
    public class AlbumModelDto : DtoBase
    {
        public string Version { get; set; }
        public string Url { get; set; }
        public string Title { get; set; }
        public DateTime? StreamStartDate { get; set; }
        public bool StreamReady { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public long OfflineDateAdded { get; set; }
        public string Type { get; set; }
        public int NumberOfVolumes { get; set; }
        public int Id { get; set; }
        public int Duration { get; set; }
        public string Cover { get; set; }
        public string Copyright { get; set; }
        public ArtistModelDto[] Artists { get; set; }
        public ArtistModelDto Artist { get; set; }
        public bool AllowStreaming { get; set; }
        public int NumberOfTracks { get; set; }
        public bool PremiumStreamingOnly { get; set; }
    }
}