﻿namespace AutomationEtoroTask.Bl.DTOs
{
    public class ArtistModelDto : DtoBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Picture { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
    }
}