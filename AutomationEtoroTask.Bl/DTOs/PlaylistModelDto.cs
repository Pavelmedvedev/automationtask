﻿using System;

namespace AutomationEtoroTask.Bl.DTOs
{
    public class PlaylistModelDto : DtoBase
    {
        public DateTime? Created { get; set; }
        public string Description { get; set; }
        public int Duration { get; set; }
        public int Id { get; set; }
        public string Image { get; set; }
        public DateTime? LastUpdated { get; set; }
        public int NumberOfTracks { get; set; }
        public long OfflineDateAdded { get; set; }
        public string Title { get; set; }
        public string Uuid { get; set; }
        public string Url { get; set; }
        public bool PublicPlaylist { get; set; }
    }
}