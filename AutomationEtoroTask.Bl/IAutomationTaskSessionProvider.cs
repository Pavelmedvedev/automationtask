﻿using AutomationEtoroTask.Bl.SessionProvider;

namespace AutomationEtoroTask.Bl
{
    public interface IAutomationTaskSessionProvider :   IOpenSessionPlaylist, IOpenSessionFavoritePlaylist,
        IOpenSessionPlaylistTracks, IOpenSessionLogout
    {

    }
}