﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenTidl.Enums;
using OpenTidl.Models;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl
{
    public interface IOpenSession
    {
        string CountryCode { get; }
        LoginModel LoginResult { get; }
        int UserId { get; }
        string SessionId { get; }

        EmptyModel AddFavoriteAlbum(int albumId, int? timeout);
        Task<EmptyModel> AddFavoriteAlbum(int albumId);
        Task<EmptyModel> AddFavoriteArtist(int artistId);
        EmptyModel AddFavoriteArtist(int artistId, int? timeout);
        Task<EmptyModel> AddFavoritePlaylist(string playlistUuid);
        EmptyModel AddFavoritePlaylist(string playlistUuid, int? timeout);
        Task<EmptyModel> AddFavoriteTrack(int trackId);
        EmptyModel AddFavoriteTrack(int trackId, int? timeout);
        Task<EmptyModel> AddPlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> trackIds, int toIndex = 0);
        EmptyModel AddPlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> trackIds, int? toIndex, int? timeout);
        PlaylistModel CreateUserPlaylist(string title, int? timeout);
        Task<PlaylistModel> CreateUserPlaylist(string title);
        Task<EmptyModel> DeletePlaylist(string playlistUuid, string playlistETag);
        EmptyModel DeletePlaylist(string playlistUuid, string playlistETag, int? timeout);
        Task<EmptyModel> DeletePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices);
        EmptyModel DeletePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices, int? timeout);
        Task<ClientModel> GetClient();
        ClientModel GetClient(int? timeout);
        JsonList<JsonListItem<AlbumModel>> GetFavoriteAlbums(int? limit, int? timeout);
        Task<JsonList<JsonListItem<AlbumModel>>> GetFavoriteAlbums(int limit = 9999);
        Task<JsonList<JsonListItem<ArtistModel>>> GetFavoriteArtists(int limit = 9999);
        JsonList<JsonListItem<ArtistModel>> GetFavoriteArtists(int? limit, int? timeout);
        JsonList<JsonListItem<PlaylistModel>> GetFavoritePlaylists(int? limit, int? timeout);
        Task<JsonList<JsonListItem<PlaylistModel>>> GetFavoritePlaylists(int limit = 9999);
        Task<JsonList<JsonListItem<TrackModel>>> GetFavoriteTracks(int limit = 9999);
        JsonList<JsonListItem<TrackModel>> GetFavoriteTracks(int? limit, int? timeout);
        PlaylistModel GetPlaylist(string playlistUuid, int? timeout);
        Task<PlaylistModel> GetPlaylist(string playlistUuid);
        JsonList<TrackModel> GetPlaylistTracks(string playlistUuid, int? offset, int? limit, int? timeout);
        Task<JsonList<TrackModel>> GetPlaylistTracks(string playlistUuid, int offset = 0, int limit = 9999);
        SessionModel GetSession(int? timeout);
        Task<SessionModel> GetSession();
        Task<StreamUrlModel> GetTrackOfflineUrl(int trackId, SoundQuality soundQuality);
        StreamUrlModel GetTrackOfflineUrl(int trackId, SoundQuality soundQuality, int? timeout);
        Task<StreamUrlModel> GetTrackStreamUrl(int trackId, SoundQuality soundQuality, string playlistUuid);
        StreamUrlModel GetTrackStreamUrl(int trackId, SoundQuality soundQuality, string playlistUuid, int? timeout);
        Task<UserModel> GetUser();
        UserModel GetUser(int? timeout);
        Task<JsonList<ClientModel>> GetUserClients(ClientFilter filter, int limit = 9999);
        JsonList<ClientModel> GetUserClients(ClientFilter filter, int? limit, int? timeout);
        JsonList<PlaylistModel> GetUserPlaylists(int? limit, int? timeout);
        Task<JsonList<PlaylistModel>> GetUserPlaylists(int limit = 9999);
        UserSubscriptionModel GetUserSubscription(int? timeout);
        Task<UserSubscriptionModel> GetUserSubscription();
        Task<VideoModel> GetVideo(int videoId);
        VideoModel GetVideo(int videoId, int? timeout);
        VideoStreamUrlModel GetVideoStreamUrl(int videoId, VideoQuality videoQuality, int? timeout);
        Task<VideoStreamUrlModel> GetVideoStreamUrl(int videoId, VideoQuality videoQuality);
        EmptyModel Logout(int? timeout);
        Task<EmptyModel> Logout();
        Task<EmptyModel> MovePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices, int toIndex = 0);
        EmptyModel MovePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices, int? toIndex, int? timeout);
        EmptyModel RemoveFavoriteAlbum(int albumId, int? timeout);
        Task<EmptyModel> RemoveFavoriteAlbum(int albumId);
        EmptyModel RemoveFavoriteArtist(int artistId, int? timeout);
        Task<EmptyModel> RemoveFavoriteArtist(int artistId);
        EmptyModel RemoveFavoritePlaylist(string playlistUuid, int? timeout);
        Task<EmptyModel> RemoveFavoritePlaylist(string playlistUuid);
        EmptyModel RemoveFavoriteTrack(int trackId, int? timeout);
        Task<EmptyModel> RemoveFavoriteTrack(int trackId);
        EmptyModel UpdatePlaylist(string playlistUuid, string playlistETag, string title, int? timeout);
        Task<EmptyModel> UpdatePlaylist(string playlistUuid, string playlistETag, string title);
    }
}