﻿using AutomationEtoroTask.Bl.SessionProvider;
using OpenTidl.Methods;

namespace AutomationEtoroTask.Bl
{
    public interface IOpenSessionProviderFactory
    {
        IAutomationTaskSessionProvider Create(OpenTidlSession openTidlSession);
    }
}