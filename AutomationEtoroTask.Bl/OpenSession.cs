﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenTidl.Enums;
using OpenTidl.Methods;
using OpenTidl.Models;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl
{
    internal class OpenSession : IOpenSession
    {
        private readonly OpenTidlSession _openTidlSession;

        public OpenSession(OpenTidlSession openTidlSession)
        {
            _openTidlSession = openTidlSession;
        }

        public string CountryCode => _openTidlSession.CountryCode;

        public LoginModel LoginResult => _openTidlSession.LoginResult;

        public int UserId => _openTidlSession.UserId;

        public string SessionId => _openTidlSession.SessionId;

        public EmptyModel AddFavoriteAlbum(int albumId, int? timeout)
        {
            return _openTidlSession.AddFavoriteAlbum(albumId, timeout);
        }

        public Task<EmptyModel> AddFavoriteAlbum(int albumId)
        {
            return _openTidlSession.AddFavoriteAlbum(albumId);
        }

        public Task<EmptyModel> AddFavoriteArtist(int artistId)
        {
            return _openTidlSession.AddFavoriteArtist(artistId);
        }

        public EmptyModel AddFavoriteArtist(int artistId, int? timeout)
        {
            return _openTidlSession.AddFavoriteArtist(artistId, timeout);
        }

        public Task<EmptyModel> AddFavoritePlaylist(string playlistUuid)
        {
            return _openTidlSession.AddFavoritePlaylist(playlistUuid);
        }

        public EmptyModel AddFavoritePlaylist(string playlistUuid, int? timeout)
        {
            return _openTidlSession.AddFavoritePlaylist(playlistUuid, timeout);
        }

        public Task<EmptyModel> AddFavoriteTrack(int trackId)
        {
            return _openTidlSession.AddFavoriteTrack(trackId);
        }

        public EmptyModel AddFavoriteTrack(int trackId, int? timeout)
        {
            return _openTidlSession.AddFavoriteTrack(trackId, timeout);
        }

        public Task<EmptyModel> AddPlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> trackIds,
            int toIndex = 0)
        {
            return _openTidlSession.AddPlaylistTracks(playlistUuid, playlistETag, trackIds, toIndex);
        }

        public EmptyModel AddPlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> trackIds,
            int? toIndex,
            int? timeout)
        {
            return _openTidlSession.AddPlaylistTracks(playlistUuid, playlistETag, trackIds, toIndex, timeout);
        }

        public PlaylistModel CreateUserPlaylist(string title, int? timeout)
        {
            return _openTidlSession.CreateUserPlaylist(title, timeout);
        }

        public Task<PlaylistModel> CreateUserPlaylist(string title)
        {
            return _openTidlSession.CreateUserPlaylist(title);
        }

        public Task<EmptyModel> DeletePlaylist(string playlistUuid, string playlistETag)
        {
            return _openTidlSession.DeletePlaylist(playlistUuid, playlistETag);
        }

        public EmptyModel DeletePlaylist(string playlistUuid, string playlistETag, int? timeout)
        {
            return _openTidlSession.DeletePlaylist(playlistUuid, playlistETag, timeout);
        }

        public Task<EmptyModel> DeletePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices)
        {
            return _openTidlSession.DeletePlaylistTracks(playlistUuid, playlistETag, indices);
        }

        public EmptyModel DeletePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices,
            int? timeout)
        {
            return _openTidlSession.DeletePlaylistTracks(playlistUuid, playlistETag, indices, timeout);
        }

        public Task<ClientModel> GetClient()
        {
            return _openTidlSession.GetClient();
        }

        public ClientModel GetClient(int? timeout)
        {
            return _openTidlSession.GetClient(timeout);
        }

        public JsonList<JsonListItem<AlbumModel>> GetFavoriteAlbums(int? limit, int? timeout)
        {
            return _openTidlSession.GetFavoriteAlbums(limit, timeout);
        }

        public Task<JsonList<JsonListItem<AlbumModel>>> GetFavoriteAlbums(int limit = 9999)
        {
            return _openTidlSession.GetFavoriteAlbums(limit);
        }

        public Task<JsonList<JsonListItem<ArtistModel>>> GetFavoriteArtists(int limit = 9999)
        {
            return _openTidlSession.GetFavoriteArtists(limit);
        }

        public JsonList<JsonListItem<ArtistModel>> GetFavoriteArtists(int? limit, int? timeout)
        {
            return _openTidlSession.GetFavoriteArtists(limit, timeout);
        }

        public JsonList<JsonListItem<PlaylistModel>> GetFavoritePlaylists(int? limit, int? timeout)
        {
            return _openTidlSession.GetFavoritePlaylists(limit, timeout);
        }

        public Task<JsonList<JsonListItem<PlaylistModel>>> GetFavoritePlaylists(int limit = 9999)
        {
            return _openTidlSession.GetFavoritePlaylists(limit);
        }

        public Task<JsonList<JsonListItem<TrackModel>>> GetFavoriteTracks(int limit = 9999)
        {
            return _openTidlSession.GetFavoriteTracks(limit);
        }

        public JsonList<JsonListItem<TrackModel>> GetFavoriteTracks(int? limit, int? timeout)
        {
            return _openTidlSession.GetFavoriteTracks(limit, timeout);
        }

        public PlaylistModel GetPlaylist(string playlistUuid, int? timeout)
        {
            return _openTidlSession.GetPlaylist(playlistUuid, timeout);
        }

        public Task<PlaylistModel> GetPlaylist(string playlistUuid)
        {
            return _openTidlSession.GetPlaylist(playlistUuid);
        }

        public JsonList<TrackModel> GetPlaylistTracks(string playlistUuid, int? offset, int? limit, int? timeout)
        {
            return _openTidlSession.GetPlaylistTracks(playlistUuid, offset, limit, timeout);
        }

        public Task<JsonList<TrackModel>> GetPlaylistTracks(string playlistUuid, int offset = 0, int limit = 9999)
        {
            return _openTidlSession.GetPlaylistTracks(playlistUuid, offset, limit);
        }

        public SessionModel GetSession(int? timeout)
        {
            return _openTidlSession.GetSession(timeout);
        }

        public Task<SessionModel> GetSession()
        {
            return _openTidlSession.GetSession();
        }

        public Task<StreamUrlModel> GetTrackOfflineUrl(int trackId, SoundQuality soundQuality)
        {
            return _openTidlSession.GetTrackOfflineUrl(trackId, soundQuality);
        }

        public StreamUrlModel GetTrackOfflineUrl(int trackId, SoundQuality soundQuality, int? timeout)
        {
            return _openTidlSession.GetTrackOfflineUrl(trackId, soundQuality, timeout);
        }

        public Task<StreamUrlModel> GetTrackStreamUrl(int trackId, SoundQuality soundQuality, string playlistUuid)
        {
            return _openTidlSession.GetTrackStreamUrl(trackId, soundQuality, playlistUuid);
        }

        public StreamUrlModel GetTrackStreamUrl(int trackId, SoundQuality soundQuality, string playlistUuid,
            int? timeout)
        {
            return _openTidlSession.GetTrackStreamUrl(trackId, soundQuality, playlistUuid, timeout);
        }

        public Task<UserModel> GetUser()
        {
            return _openTidlSession.GetUser();
        }

        public UserModel GetUser(int? timeout)
        {
            return _openTidlSession.GetUser(timeout);
        }

        public Task<JsonList<ClientModel>> GetUserClients(ClientFilter filter, int limit = 9999)
        {
            return _openTidlSession.GetUserClients(filter, limit);
        }

        public JsonList<ClientModel> GetUserClients(ClientFilter filter, int? limit, int? timeout)
        {
            return _openTidlSession.GetUserClients(filter, limit, timeout);
        }

        public JsonList<PlaylistModel> GetUserPlaylists(int? limit, int? timeout)
        {
            return _openTidlSession.GetUserPlaylists(limit, timeout);
        }

        public Task<JsonList<PlaylistModel>> GetUserPlaylists(int limit = 9999)
        {
            return _openTidlSession.GetUserPlaylists(limit);
        }

        public UserSubscriptionModel GetUserSubscription(int? timeout)
        {
            return _openTidlSession.GetUserSubscription(timeout);
        }

        public Task<UserSubscriptionModel> GetUserSubscription()
        {
            return _openTidlSession.GetUserSubscription();
        }

        public Task<VideoModel> GetVideo(int videoId)
        {
            return _openTidlSession.GetVideo(videoId);
        }

        public VideoModel GetVideo(int videoId, int? timeout)
        {
            return _openTidlSession.GetVideo(videoId, timeout);
        }

        public VideoStreamUrlModel GetVideoStreamUrl(int videoId, VideoQuality videoQuality, int? timeout)
        {
            return _openTidlSession.GetVideoStreamUrl(videoId, videoQuality, timeout);
        }

        public Task<VideoStreamUrlModel> GetVideoStreamUrl(int videoId, VideoQuality videoQuality)
        {
            return _openTidlSession.GetVideoStreamUrl(videoId, videoQuality);
        }

        public EmptyModel Logout(int? timeout)
        {
            return _openTidlSession.Logout(timeout);
        }

        public Task<EmptyModel> Logout()
        {
            return _openTidlSession.Logout();
        }

        public Task<EmptyModel> MovePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices,
            int toIndex = 0)
        {
            return _openTidlSession.MovePlaylistTracks(playlistUuid, playlistETag, indices, toIndex);
        }

        public EmptyModel MovePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices,
            int? toIndex,
            int? timeout)
        {
            return _openTidlSession.MovePlaylistTracks(playlistUuid, playlistETag, indices, toIndex, timeout);
        }

        public EmptyModel RemoveFavoriteAlbum(int albumId, int? timeout)
        {
            return _openTidlSession.RemoveFavoriteAlbum(albumId, timeout);
        }

        public Task<EmptyModel> RemoveFavoriteAlbum(int albumId)
        {
            return _openTidlSession.RemoveFavoriteAlbum(albumId);
        }

        public EmptyModel RemoveFavoriteArtist(int artistId, int? timeout)
        {
            return _openTidlSession.RemoveFavoriteArtist(artistId, timeout);
        }

        public Task<EmptyModel> RemoveFavoriteArtist(int artistId)
        {
            return _openTidlSession.RemoveFavoriteArtist(artistId);
        }

        public EmptyModel RemoveFavoritePlaylist(string playlistUuid, int? timeout)
        {
            return _openTidlSession.RemoveFavoritePlaylist(playlistUuid, timeout);
        }

        public Task<EmptyModel> RemoveFavoritePlaylist(string playlistUuid)
        {
            return _openTidlSession.RemoveFavoritePlaylist(playlistUuid);
        }

        public EmptyModel RemoveFavoriteTrack(int trackId, int? timeout)
        {
            return _openTidlSession.RemoveFavoriteTrack(trackId, timeout);
        }

        public Task<EmptyModel> RemoveFavoriteTrack(int trackId)
        {
            return _openTidlSession.RemoveFavoriteTrack(trackId);
        }

        public EmptyModel UpdatePlaylist(string playlistUuid, string playlistETag, string title, int? timeout)
        {
            return _openTidlSession.UpdatePlaylist(playlistUuid, playlistETag, title, timeout);
        }

        public Task<EmptyModel> UpdatePlaylist(string playlistUuid, string playlistETag, string title)
        {
            return _openTidlSession.UpdatePlaylist(playlistUuid, playlistETag, title);
        }
    }
}