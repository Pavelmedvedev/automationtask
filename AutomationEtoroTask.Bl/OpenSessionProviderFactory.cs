﻿using AutomationEtoroTask.Bl.SessionProvider;
using AutoMapper;
using OpenTidl.Methods;

namespace AutomationEtoroTask.Bl
{
    public class OpenSessionProviderFactory : IOpenSessionProviderFactory
    {
        private readonly IMapper _mapper;

        public OpenSessionProviderFactory(IMapper mapper)
        {
            _mapper = mapper;
        }
        public IAutomationTaskSessionProvider Create(OpenTidlSession openTidlSession)
        {
            var openSession = new OpenSession(openTidlSession);
            var openSessionProvider = new AutomationTaskSessionProvider(new OpenSessionPlaylist(openSession, _mapper),new OpenSessionFavoritePlaylist(openSession, _mapper),new OpenSessionPlaylistTracks(openSession, _mapper),new OpenSessionLogout(openSession, _mapper)   );
            return openSessionProvider;
        }
    }
}