﻿using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using OpenTidl.Enums;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public interface IOpenSessionAdministrative
    {
        string CountryCode { get; }
        LoginModelDto LoginResult { get; }
        int UserId { get; }
        string SessionId { get; }
        Task<ClientModelDto> GetClient();
        ClientModelDto GetClient(int? timeout);
        SessionModelDto GetSession(int? timeout);
        Task<SessionModelDto> GetSession();
        Task<UserModelDto> GetUser();
        UserModelDto GetUser(int? timeout);
        Task<JsonList<ClientModelDto>> GetUserClients(ClientFilter filter, int limit = 9999);
        JsonList<ClientModelDto> GetUserClients(ClientFilter filter, int? limit, int? timeout);
    }
}