﻿using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public interface IOpenSessionFavoriteAlbum
    {
        EmptyModelDto AddFavoriteAlbum(int albumId, int? timeout);
        Task<EmptyModelDto> AddFavoriteAlbum(int albumId);
        JsonList<JsonListItem<AlbumModelDto>> GetFavoriteAlbums(int? limit, int? timeout);
        Task<JsonList<JsonListItem<AlbumModelDto>>> GetFavoriteAlbums(int limit = 9999);
        EmptyModelDto RemoveFavoriteAlbum(int albumId, int? timeout);
        Task<EmptyModelDto> RemoveFavoriteAlbum(int albumId);
    }
}