﻿using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public interface IOpenSessionFavoriteArtist
    {
        Task<EmptyModelDto> AddFavoriteArtist(int artistId);
        EmptyModelDto AddFavoriteArtist(int artistId, int? timeout);
        Task<JsonList<JsonListItem<ArtistModelDto>>> GetFavoriteArtists(int limit = 9999);
        JsonList<JsonListItem<ArtistModelDto>> GetFavoriteArtists(int? limit, int? timeout);
        EmptyModelDto RemoveFavoriteArtist(int artistId, int? timeout);
        Task<EmptyModelDto> RemoveFavoriteArtist(int artistId);
    }
}