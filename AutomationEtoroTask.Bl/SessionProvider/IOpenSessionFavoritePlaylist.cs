﻿using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public interface IOpenSessionFavoritePlaylist
    {
        Task<EmptyModelDto> AddFavoritePlaylist(string playlistUuid);
        EmptyModelDto AddFavoritePlaylist(string playlistUuid, int? timeout);
        JsonList<JsonListItem<PlaylistModelDto>> GetFavoritePlaylists(int? limit, int? timeout);
        Task<JsonList<JsonListItem<PlaylistModelDto>>> GetFavoritePlaylists(int limit = 9999);
        EmptyModelDto RemoveFavoritePlaylist(string playlistUuid, int? timeout);
        Task<EmptyModelDto> RemoveFavoritePlaylist(string playlistUuid);
    }
}