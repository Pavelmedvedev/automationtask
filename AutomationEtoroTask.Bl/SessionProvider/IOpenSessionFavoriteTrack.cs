﻿using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public interface IOpenSessionFavoriteTrack
    {
        Task<EmptyModelDto> AddFavoriteTrack(int trackId);
        EmptyModelDto AddFavoriteTrack(int trackId, int? timeout);
        Task<JsonList<JsonListItem<TrackModelDto>>> GetFavoriteTracks(int limit = 9999);
        JsonList<JsonListItem<TrackModelDto>> GetFavoriteTracks(int? limit, int? timeout);
        EmptyModelDto RemoveFavoriteTrack(int trackId, int? timeout);
        Task<EmptyModelDto> RemoveFavoriteTrack(int trackId);
    }
}