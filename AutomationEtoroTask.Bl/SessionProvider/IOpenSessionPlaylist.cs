﻿using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public interface IOpenSessionPlaylist 
    {
        PlaylistModelDto CreateUserPlaylist(string title, int? timeout);
        Task<PlaylistModelDto> CreateUserPlaylist(string title);
        Task<EmptyModelDto> DeletePlaylist(string playlistUuid, string playlistETag);
        EmptyModelDto DeletePlaylist(string playlistUuid, string playlistETag, int? timeout);
        EmptyModelDto UpdatePlaylist(string playlistUuid, string playlistETag, string title, int? timeout);
        Task<EmptyModelDto> UpdatePlaylist(string playlistUuid, string playlistETag, string title);
        JsonList<PlaylistModelDto> GetUserPlaylists(int? limit, int? timeout);
        Task<JsonList<PlaylistModelDto>> GetUserPlaylists(int limit = 9999);
        PlaylistModelDto GetPlaylist(string playlistUuid, int? timeout);
        Task<PlaylistModelDto> GetPlaylist(string playlistUuid);
    }
}