﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public interface IOpenSessionPlaylistTracks
    {
        Task<EmptyModelDto> AddPlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> trackIds, int toIndex = 0);
        EmptyModelDto AddPlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> trackIds, int? toIndex, int? timeout);
        Task<EmptyModelDto> DeletePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices);
        EmptyModelDto DeletePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices, int? timeout);
        JsonList<TrackModelDto> GetPlaylistTracks(string playlistUuid, int? offset, int? limit, int? timeout);
        Task<JsonList<TrackModelDto>> GetPlaylistTracks(string playlistUuid, int offset = 0, int limit = 9999);
        Task<EmptyModelDto> MovePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices, int toIndex = 0);
        EmptyModelDto MovePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices, int? toIndex, int? timeout);
    }
}