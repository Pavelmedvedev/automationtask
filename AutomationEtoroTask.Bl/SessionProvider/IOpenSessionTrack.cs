﻿using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using OpenTidl.Enums;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public interface IOpenSessionTrack
    {
        Task<StreamUrlModelDto> GetTrackOfflineUrl(int trackId, SoundQuality soundQuality);
        StreamUrlModelDto GetTrackOfflineUrl(int trackId, SoundQuality soundQuality, int? timeout);

        Task<StreamUrlModelDto> GetTrackStreamUrl(int trackId, SoundQuality soundQuality, string playlistUuid);
        StreamUrlModelDto GetTrackStreamUrl(int trackId, SoundQuality soundQuality, string playlistUuid, int? timeout);
    }
}