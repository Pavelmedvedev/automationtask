﻿using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public interface IOpenSessionUserSubscription
    {
        UserSubscriptionModelDto GetUserSubscription(int? timeout);
        Task<UserSubscriptionModelDto> GetUserSubscription();
    }
}