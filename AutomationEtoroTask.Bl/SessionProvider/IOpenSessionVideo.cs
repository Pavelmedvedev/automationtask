﻿using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using OpenTidl.Enums;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public interface IOpenSessionVideo
    {
        Task<VideoModelDto> GetVideo(int videoId);
        VideoModelDto GetVideo(int videoId, int? timeout);
        VideoStreamUrlModelDto GetVideoStreamUrl(int videoId, VideoQuality videoQuality, int? timeout);
        Task<VideoStreamUrlModelDto> GetVideoStreamUrl(int videoId, VideoQuality videoQuality);
    }
}