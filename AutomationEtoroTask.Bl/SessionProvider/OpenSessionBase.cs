﻿using AutoMapper;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public abstract class OpenSessionBase
    {
        protected readonly IOpenSession OpenSession;
        protected readonly IMapper Mapper;

        protected OpenSessionBase(IOpenSession openSession, IMapper mapper)
        {
            OpenSession = openSession;
            Mapper = mapper;
        }
    }
}