﻿using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using AutoMapper;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public class OpenSessionFavoritePlaylist : OpenSessionBase, IOpenSessionFavoritePlaylist
    {
        public OpenSessionFavoritePlaylist(IOpenSession openSession, IMapper mapper) : base(openSession, mapper)
        {
        }

        public async Task<EmptyModelDto> AddFavoritePlaylist(string playlistUuid)
        {
            return Mapper.Map<EmptyModelDto>(await OpenSession.AddFavoritePlaylist(playlistUuid));
        }

        public EmptyModelDto AddFavoritePlaylist(string playlistUuid, int? timeout)
        {
            return Mapper.Map<EmptyModelDto>(OpenSession.AddFavoritePlaylist(playlistUuid, timeout));
        }

        public JsonList<JsonListItem<PlaylistModelDto>> GetFavoritePlaylists(int? limit, int? timeout)
        {
            return Mapper.Map<JsonList<JsonListItem<PlaylistModelDto>>>(OpenSession.GetFavoritePlaylists(limit, timeout));
        }

        public async Task<JsonList<JsonListItem<PlaylistModelDto>>> GetFavoritePlaylists(int limit = 9999)
        {
            return Mapper.Map<JsonList<JsonListItem<PlaylistModelDto>>>(await OpenSession.GetFavoritePlaylists(limit));
        }

        public EmptyModelDto RemoveFavoritePlaylist(string playlistUuid, int? timeout)
        {
            return Mapper.Map<EmptyModelDto>(OpenSession.RemoveFavoritePlaylist(playlistUuid, timeout));
        }

        public async Task<EmptyModelDto> RemoveFavoritePlaylist(string playlistUuid)
        {
            return Mapper.Map<EmptyModelDto>(await OpenSession.RemoveFavoritePlaylist(playlistUuid));
        }
    }
}