﻿using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using AutoMapper;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public class OpenSessionLogout : OpenSessionBase, IOpenSessionLogout
    {
        public OpenSessionLogout(IOpenSession openSession, IMapper mapper) : base(openSession, mapper)
        {
        }

        public EmptyModelDto Logout(int? timeout)
        {
            return Mapper.Map<EmptyModelDto>(OpenSession.Logout(timeout));
        }

        public async Task<EmptyModelDto> Logout()
        {
            return Mapper.Map<EmptyModelDto>(await OpenSession.Logout());
        }
    }
}