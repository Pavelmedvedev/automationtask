﻿using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using AutoMapper;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public class OpenSessionPlaylist : OpenSessionBase, IOpenSessionPlaylist
    {
        public OpenSessionPlaylist(IOpenSession openSession, IMapper mapper) : base(openSession, mapper)
        {
        }

        public PlaylistModelDto CreateUserPlaylist(string title, int? timeout)
        {
            return Mapper.Map<PlaylistModelDto>(OpenSession.CreateUserPlaylist(title, timeout));
        }

        public async Task<PlaylistModelDto> CreateUserPlaylist(string title)
        {
            return Mapper.Map<PlaylistModelDto>(await OpenSession.CreateUserPlaylist(title));
        }

        public async Task<EmptyModelDto> DeletePlaylist(string playlistUuid, string playlistETag)
        {
            return Mapper.Map<EmptyModelDto>(await OpenSession.DeletePlaylist(playlistUuid, playlistETag));
        }

        public EmptyModelDto DeletePlaylist(string playlistUuid, string playlistETag, int? timeout)
        {
            return Mapper.Map<EmptyModelDto>(OpenSession.DeletePlaylist(playlistUuid, playlistETag, timeout));
        }

        public EmptyModelDto UpdatePlaylist(string playlistUuid, string playlistETag, string title, int? timeout)
        {
            return Mapper.Map<EmptyModelDto>(OpenSession.UpdatePlaylist(playlistUuid, playlistETag, title, timeout));
        }

        public async Task<EmptyModelDto> UpdatePlaylist(string playlistUuid, string playlistETag, string title)
        {
            return Mapper.Map<EmptyModelDto>(await OpenSession.UpdatePlaylist(playlistUuid, playlistETag, title));
        }


        public JsonList<PlaylistModelDto> GetUserPlaylists(int? limit, int? timeout)
        {
            return Mapper.Map<JsonList<PlaylistModelDto>>(OpenSession.GetUserPlaylists(limit, timeout));
        }

        public async Task<JsonList<PlaylistModelDto>> GetUserPlaylists(int limit = 9999)
        {
            return Mapper.Map<JsonList<PlaylistModelDto>>(await OpenSession.GetUserPlaylists(limit));
        }

        public PlaylistModelDto GetPlaylist(string playlistUuid, int? timeout)
        {
            return Mapper.Map<PlaylistModelDto>(OpenSession.GetPlaylist(playlistUuid, timeout));
        }

        public async Task<PlaylistModelDto> GetPlaylist(string playlistUuid)
        {
            return Mapper.Map<PlaylistModelDto>(await OpenSession.GetPlaylist(playlistUuid));
        }
    }
}