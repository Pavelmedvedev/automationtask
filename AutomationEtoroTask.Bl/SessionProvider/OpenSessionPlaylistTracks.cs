﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutomationEtoroTask.Bl.DTOs;
using AutoMapper;
using OpenTidl.Models.Base;

namespace AutomationEtoroTask.Bl.SessionProvider
{
    public class OpenSessionPlaylistTracks : OpenSessionBase, IOpenSessionPlaylistTracks
    {
        public OpenSessionPlaylistTracks(IOpenSession openSession, IMapper mapper) : base(openSession, mapper)
        {
        }
        public async Task<EmptyModelDto> AddPlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> trackIds, int toIndex = 0)
        {
            return Mapper.Map<EmptyModelDto>(await OpenSession.AddPlaylistTracks(playlistUuid, playlistETag, trackIds, toIndex));
        }

        public EmptyModelDto AddPlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> trackIds, int? toIndex,
            int? timeout)
        {
            return Mapper.Map<EmptyModelDto>(OpenSession.AddPlaylistTracks(playlistUuid, playlistETag, trackIds, toIndex, timeout));
        }
        public async Task<EmptyModelDto> DeletePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices)
        {
            return Mapper.Map<EmptyModelDto>(await OpenSession.DeletePlaylistTracks(playlistUuid, playlistETag, indices));
        }

        public EmptyModelDto DeletePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices, int? timeout)
        {
            return Mapper.Map<EmptyModelDto>(OpenSession.DeletePlaylistTracks(playlistUuid, playlistETag, indices, timeout));
        }

        public JsonList<TrackModelDto> GetPlaylistTracks(string playlistUuid, int? offset, int? limit, int? timeout)
        {
            return Mapper.Map<JsonList<TrackModelDto>>(OpenSession.GetPlaylistTracks(playlistUuid, offset, limit, timeout));
        }

        public async Task<JsonList<TrackModelDto>> GetPlaylistTracks(string playlistUuid, int offset = 0, int limit = 9999)
        {
            return Mapper.Map<JsonList<TrackModelDto>>(await OpenSession.GetPlaylistTracks(playlistUuid, offset, limit));
        }
        public async Task<EmptyModelDto> MovePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices, int toIndex = 0)
        {
            return Mapper.Map<EmptyModelDto>(await OpenSession.MovePlaylistTracks(playlistUuid, playlistETag, indices, toIndex));
        }

        public EmptyModelDto MovePlaylistTracks(string playlistUuid, string playlistETag, IEnumerable<int> indices, int? toIndex,
            int? timeout)
        {
            return Mapper.Map<EmptyModelDto>(OpenSession.MovePlaylistTracks(playlistUuid, playlistETag, indices, toIndex, timeout));
        }
    }
}