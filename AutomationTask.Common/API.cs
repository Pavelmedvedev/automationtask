﻿using AutomationEtoroTask.Bl;
using AutomationEtoroTask.Bl.SessionProvider;
using AutoMapper;
using OpenTidl;
using Unity;

namespace AutomationTask.Common
{
    public class Api
    {
        public static ILogin CreateTidlClient(ClientConfiguration clientConfiguration)
        {
            if (clientConfiguration == null)
            {
                clientConfiguration = ClientConfiguration.Default;
            }

            var container = new UnityContainer();
            var configurationProvider = new MapperConfiguration(expression =>
            {
                var autoMapperProfiles = new AutoMapperProfiles();
                expression.AddProfile(autoMapperProfiles);
            });
            
            var mapper = new Mapper(configurationProvider);
            var openTidlClient = new OpenTidlClient(clientConfiguration);
            container.RegisterInstance(openTidlClient);
            container.RegisterInstance<IConfigurationProvider>(configurationProvider);
            container.RegisterInstance<IMapper>(mapper);

            container.RegisterType<IOpenSessionLogout, OpenSessionLogout>();
            container.RegisterType<IOpenSessionPlaylistTracks, OpenSessionPlaylistTracks>();
            container.RegisterType<IOpenSessionFavoritePlaylist, OpenSessionFavoritePlaylist>();
            container.RegisterType<IOpenSessionPlaylist, OpenSessionPlaylist>();
            container.RegisterType<IAutomationTaskSessionProvider, AutomationTaskSessionProvider>();
            container.RegisterType<IOpenSessionProviderFactory, OpenSessionProviderFactory>();
            container.RegisterType<ILogin, Login>();

            return container.Resolve<ILogin>();
        }
    }
}
