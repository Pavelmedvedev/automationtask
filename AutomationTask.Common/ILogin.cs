﻿using AutomationEtoroTask.Bl;

namespace AutomationTask.Common
{
    public interface ILogin
    {
        IAutomationTaskSessionProvider LoginWithFacebook(string accessToken);
        IAutomationTaskSessionProvider LoginWithToken(string accessToken);
        IAutomationTaskSessionProvider LoginWithTwitter(string accessToken, string accessTokenKey);
        IAutomationTaskSessionProvider LoginWithUsername(string username, string password);
    }
}