﻿using AutomationEtoroTask.Bl;
using OpenTidl;

namespace AutomationTask.Common
{
    public class Login : ILogin
    {
        private readonly OpenTidlClient _client;
        private readonly IOpenSessionProviderFactory _sessionProviderFactory;

        public Login(OpenTidlClient client, IOpenSessionProviderFactory sessionProviderFactory)
        {
            _client = client;
            _sessionProviderFactory = sessionProviderFactory;
        }

        public IAutomationTaskSessionProvider LoginWithFacebook(string accessToken)
        {
            var openTidlSession = _client.LoginWithFacebook(accessToken).Result;
            return _sessionProviderFactory.Create(openTidlSession);
        }

        public IAutomationTaskSessionProvider LoginWithToken(string accessToken)
        {
            var openTidlSession = _client.LoginWithToken(accessToken).Result;
            return _sessionProviderFactory.Create(openTidlSession);
        }

        public IAutomationTaskSessionProvider LoginWithTwitter(string accessToken, string accessTokenKey)
        {
            var openTidlSession = _client.LoginWithTwitter(accessToken, accessTokenKey).Result;
            return _sessionProviderFactory.Create(openTidlSession);
        }

        public IAutomationTaskSessionProvider LoginWithUsername(string username, string password)
        {
            var openTidlSession = _client.LoginWithUsername(username, password).Result;
            return _sessionProviderFactory.Create(openTidlSession);
        }
    }
}