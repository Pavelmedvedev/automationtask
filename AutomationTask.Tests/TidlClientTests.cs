using System.Collections.Generic;
using System.Threading.Tasks;
using AutomationEtoroTask.Bl;
using AutomationTask.Common;
using NUnit.Framework;
using OpenTidl;

namespace AutomationTask.Tests
{
    public class TidlClientTests
    {
        private IAutomationTaskSessionProvider _openSessionProvider;
        [SetUp]
        public void Setup()
        {
            _openSessionProvider = Api.CreateTidlClient(ClientConfiguration.Default)
                .LoginWithTwitter(string.Empty, string.Empty);
        }

        [Test]
        public async Task CreateUserPlaylistAsync_Verify_Title()
        {
            var title = "title";
            var playlist = await _openSessionProvider.CreateUserPlaylist(title);
            Assert.That(string.Equals(playlist.Title, title), Is.True);
        }

        [Test]
        public void CreateUserPlaylist_Verify_Title()
        {
            var title = "title";
            var timeout = 1000;
            var playlist = _openSessionProvider.CreateUserPlaylist(title, timeout);
            Assert.That(string.Equals(playlist.Title, title), Is.True);
        }

        [Test]
        public async Task AddPlaylistTracksAsync_Verify_Title()
        {
            var playlistUuid = "";
            var playlistETag = "";
            var trackIds = new List<int> { 6 };
            var toIndex = 0;
            var result = await _openSessionProvider.AddPlaylistTracks(playlistUuid, playlistETag, trackIds, toIndex);
            Assert.That(string.Equals(result.ETag, playlistETag), Is.True);
        }

        [Test]
        public void AddPlaylistTracks_Verify_Title()
        {
            var playlistUuid = "";
            var playlistETag = "";
            var trackIds = new List<int> { 6 };
            var toIndex = 0;
            var timeout = 1000;
            var result = _openSessionProvider.AddPlaylistTracks(playlistUuid, playlistETag, trackIds, toIndex, timeout);
            Assert.That(string.Equals(result.ETag, playlistETag), Is.True);
        }

        [Test]
        public async Task DeletePlaylistTracksAsync_Verify_Title()
        {
            var playlistUuid = "";
            var playlistETag = "";
            var indices = new List<int> { 6 };
            var result = await _openSessionProvider.DeletePlaylistTracks(playlistUuid, playlistETag, indices);
            Assert.That(string.Equals(result.ETag, playlistETag), Is.True);
        }

        [Test]
        public void DeletePlaylistTracks_Verify_Title()
        {
            var playlistUuid = "";
            var playlistETag = "";
            var indices = new List<int> { 6 };
            var timeout = 1000;
            var result = _openSessionProvider.DeletePlaylistTracks(playlistUuid, playlistETag, indices, timeout);
            Assert.That(string.Equals(result.ETag, playlistETag), Is.True);
        }

        [Test]
        public async Task GetUserPlaylistsAsync_Verify_Title()
        {
            var result = await _openSessionProvider.GetUserPlaylists(500);
            Assert.That(Equals(result.Items.Length, 1), Is.True);
        }

        [Test]
        public void GetUserPlaylists_Verify_Title()
        {
            var timeout = 1000;
            var result = _openSessionProvider.GetUserPlaylists(500, timeout);
            Assert.That(Equals(result.Items.Length, 1), Is.True);
        }
    }
}